# calculator

Basic calculator created using VueJS and CoreUI.

## Features

* [x] Operators -, +, /, *
* [x] Unary operators -, +
* [x] Parenthesis
* [x] Float numbers

## Evaluation Algorithms

* Finite state machine as parser
* Shunting yard algorithm to manage precedence and transform query to RPN (Reverse Polish Notation)
* RPN Solver

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run unit tests

Unit test engine: Jest
```
npm run test
```


## References

* Finite-state machine: https://en.wikipedia.org/wiki/Finite-state_machine

* Shunting Yard algorithm: https://en.wikipedia.org/wiki/Shunting-yard_algorithm

* RPN: https://en.wikipedia.org/wiki/Reverse_Polish_notation
