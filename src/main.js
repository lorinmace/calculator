import Vue from 'vue'
import App from './App.vue'

// Install CoreUI
import CoreuiVue from '@coreui/vue'
Vue.use(CoreuiVue)
require('../node_modules/@coreui/coreui/scss/coreui.scss')

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
