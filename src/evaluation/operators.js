class Operator {
    constructor(resolution, precedence) {
        // Function to resolve this operation
        this.resolution = resolution

        // Resolution priority
        this.precedence = precedence
    }
}

// Warning: Following operators are left-associative.
// If you intend to add right-associative operators, 
// you need to change the shunting yard algorithm
let operators = {
    "+": new Operator(
        function(b, a){ return a+b },
        3
    ),

    "-": new Operator(
        function(b, a){ return a-b },
        3
    ),

    "*": new Operator(
        function(b, a){ return a*b },
        4
    ),

    "/": new Operator(
        function(b, a){
            if (b == 0)
                throw "Division by Zero error !"
    
            return a/b
        },
        4
    )
}

let unary_operators = {
    "+": new Operator(
        function(a){ return a },
        3
    ),

    "-": new Operator(
        function(a){ return -a },
        3
    )
}

export { operators, unary_operators }