import evaluate from './evaluate'

test('Empty', () => {
  expect(evaluate("")).toBe(0)
})

test('Invalid char', () => {
  expect(() => evaluate("$")).toThrow("Invalid character $ at position 1 !")
  expect(() => evaluate("24.4+10*1$+45")).toThrow("Invalid character $ at position 10 !")
})

test('Number', () => {
  expect(evaluate("1")).toBe(1)
  expect(evaluate("20")).toBe(20)
  expect(evaluate("456")).toBe(456)
})

test('Invalid number', () => {
  expect(() => evaluate(".")).toThrow(". is not a correct number !")
  expect(() => evaluate("..2")).toThrow("..2 is not a correct number !")
})

test('Negative', () => {
  expect(evaluate("-1")).toBe(-1)
  expect(evaluate("-20")).toBe(-20)
  expect(evaluate("-456")).toBe(-456)
})

test('Float', () => {
  expect(evaluate("1.2")).toBe(1.2)
  expect(evaluate("2.3")).toBe(2.3)
})

test('Addition', () => {
  expect(evaluate("1+1")).toBe(2)
  expect(evaluate("1+2")).toBe(3)
  expect(evaluate("3+2")).toBe(5)
})


test('Soustraction', () => {
  expect(evaluate("1-1")).toBe(0)
  expect(evaluate("10-10")).toBe(0)
  expect(evaluate("7-5")).toBe(2)
  expect(evaluate("3-5")).toBe(-2)
})

test('Multiplication', () => {
  expect(evaluate("2*2")).toBe(4)
  expect(evaluate("1*1")).toBe(1)
})

test('Division', () => {
  expect(evaluate("10/2")).toBe(5)
  expect(evaluate("5/2")).toBe(2.5)
})

test('Division by 0', () => {
  expect(() => evaluate("10/0")).toThrow("Division by Zero error !")
})

test('Precedence', () => {
  expect(evaluate("10+2*4")).toBe(18)
  expect(evaluate("10+4*2")).toBe(18)
  expect(evaluate("2*10+4")).toBe(24)
  expect(evaluate("10*2+4")).toBe(24)
  expect(evaluate("10/2*4")).toBe(20)
  expect(evaluate("10/2-4")).toBe(1)
})

test('Complex', () => {
  expect(evaluate("2+3*4/2-4/2")).toBe(6)
  expect(evaluate("4/2-3.0*4/2.0+1.1")).toBe(-2.9)
  expect(evaluate("3*-4+2")).toBe(-10)
  expect(evaluate("(2+3)*5")).toBe(25)
})

test('Requirement 1', () => {
  expect(evaluate("5+1")).toBe(6)
})

test('Requirement 2', () => {
  expect(evaluate("8-12")).toBe(-4)
})

test('Requirement 3', () => {
  expect(evaluate("-3*-20")).toBe(60)
})

test('Requirement 4', () => {
  expect(evaluate("12/3")).toBe(4)
})

test('Invalid query', () => {
  expect(() => evaluate("12//3")).toThrow("Invalid query at position 4 !")
  expect(() => evaluate("12+*3")).toThrow("Invalid query at position 4 !")
  expect(() => evaluate("*3")).toThrow("Invalid query !")
  expect(() => evaluate("*-3")).toThrow("Invalid query !")
})

test('Parenthesis', () => {
  expect(evaluate("(10)")).toBe(10)
  expect(evaluate("(10+1)")).toBe(11)
  expect(evaluate("2*(10+1)")).toBe(22)
  expect(evaluate("(((((((((((((((((((((((((((()+()-()*())))))))))))))))))))))))))))")).toBe(0)
  expect(evaluate("(3+2)(2)")).toBe(10)
  expect(evaluate("4(2)")).toBe(8)
  expect(evaluate("(4)2")).toBe(8)
})

test('Mismatched parenthesis', () => {
  expect(() => evaluate("(2+1")).toThrow("Mismatched parenthesis !")
  expect(() => evaluate("((12)+3")).toThrow("Mismatched parenthesis !")
})

test('Query too long', () => {
  expect(() => evaluate("a".repeat(501))).toThrow("Query too long")
})