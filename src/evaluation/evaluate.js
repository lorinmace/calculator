import { operators, unary_operators } from './operators'

// Add last() operator for arrays
if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1]
    }
}

// Define is a char is part of a number
function is_number_part(value){
    return value === "." || !isNaN(value)
}

// Transform number parts onto number
// Return value and number end index on query
function parseNumber(query, index){
    var number = ""
    while (index < query.length 
            && is_number_part(query[index])) {
        number+=query[index]
        index++
    }

    let value = parseFloat(number)

    if (isNaN(value))
        throw number + " is not a correct number !"

    return {
        endIndex:index-1, 
        value:value
    }
}

// Find the end of the parenthesis and evaluate it recursively
function parseParenthesis(query, index){
    var count = 0

    for (var endIndex = index; endIndex < query.length; endIndex++){
        switch (query[endIndex]) {
            case '(': 
                count += 1
                break
            case ')':
                count -= 1
                break
            default:
                break
        }
        if (count == 0) 
            break
    }

    if (count > 0)
        throw "Mismatched parenthesis !"

    let subquery = query.slice(index+1,endIndex)
    
    return {
        endIndex:endIndex, 
        value:evaluate(subquery)
    }
}

// State machine that handles parse
function* parseQuery(query){
    // Store unary operators modifications
    var number_modifiers = []

    var number

    // Warning: number parsing may skip indexes in the loop
    for (var index = 0; index < query.length; index++) {
        if (query[index] === "(") {
            number = parseParenthesis(query, index)

            yield {value:number.value, isNumber: true}

            index = number.endIndex

        } else if (is_number_part(query[index])) {
            number = parseNumber(query, index)
            var value = number.value

            // Apply unary operators
            for (var modifier of number_modifiers) 
                value = modifier.resolution(value)

            number_modifiers = []

            yield {value, isNumber: true}

            // Jump to next next symbol that is not part if this number
            index = number.endIndex

        // Unary operators
        } else if (query[index] in unary_operators &&
                (query[index-1] === undefined || query[index-1] in operators)) {
                
            number_modifiers.push(unary_operators[query[index]])
            
        // Operators
        } else if (query[index] in operators) {    
            
            // Previous needs to be a number
            if (query[index-1] in operators)
                throw "Invalid query at position " + (index + 1) + " !"
 
            yield {value:operators[query[index]], isNumber: false}
        
        } else {
            throw "Invalid character "+query[index]+ " at position " + (index + 1) + " !"
        }
    }
}

/*
This algorithm to transform "classic" query format to RPN format
Simplified algorithm:

while token := tokens:
    if the token is an operator then:
        while (
            there is an operator at the top of the operator stack
            and the operator at the top of the operator 
                    stack has greater or equal precedence)
            ):
                pop operators from the operator stack onto the output queue
    push token onto the operator stack.
Depop operator stack onto output

*/
function* shuntingYard(query) {
    var stack = []

    // Warning: number parsing may skip indexes in the loop
    for (let element of query) {
        if (element.isNumber) {
            yield element
        } else {
            while(stack.last() != undefined && 
                    stack.last().value.precedence >= element.value.precedence) {
                yield stack.pop()
            }
            stack.push(element)
        }
    }
    
    while (stack.length > 0) 
        yield stack.pop()

}

// Reverse Polish Notation solver 
function evaluateRPN(query) {
    var stack = []

    for (let element of query){
        if (element.isNumber) {
            stack.push(element.value)
        } else {
            if (stack.length < 2)
                throw "Invalid query !"
            stack.push(element.value.resolution(stack.pop(), stack.pop()))
        }
    }

    // Empty query
    if (stack.length == 0) 
        return 0

    // Unspecified operators are "*"
    while (stack.length >= 2)
        stack.push(operators["*"].resolution(stack.pop(), stack.pop()))

    return stack[0]
}

function evaluate(query) {

    // Avoid reaching recursion limit
    if (query.length > 500) 
        throw 'Query too long !'

    // Parse query and handle unary operators
    let parsedQuery = parseQuery(query)

    // Transform query to RPN query
    let rpnQuery = shuntingYard(parsedQuery)

    // Evaluate RPN Query
    return evaluateRPN(rpnQuery)
}

export default evaluate